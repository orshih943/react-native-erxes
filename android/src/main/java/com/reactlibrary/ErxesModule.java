package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.newmedia.erxeslibrary.configuration.Config;

public class ErxesModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private Config config = null;
    public ErxesModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "Erxes";
    }

    @ReactMethod
    public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
        // TODO: Implement some actually useful functionality
        callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + stringArgument);


    }

    @ReactMethod
    public void init(String brandid, String apihost,  String subscriptionhost ,String uploadhost,Callback callback) {
        config = new Config.Builder(brandid)
                .setApiHost(apihost)
                .setSubscriptionHost(subscriptionhost)
                .setUploadHost(uploadhost)
                .build(reactContext);
    }

    @ReactMethod
    public void start() {
        if(config!=null)
        config.Start();
    }
}
